using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
enum GameDifficulty
{
    Easy,
    Medium,
    Hard
}
public class SceneController : MonoBehaviour
{
    [SerializeField] private GameObject gameMenu;
    [SerializeField] private GameObject winMenu;
    [SerializeField] private Text scoreLabel;
    [SerializeField] private Text finalScore;
    [SerializeField] int numberOfWin;
    private int _winPoints = 0;
    private int _score = 0;

    private MemoryCard _firstRevealed;
    private MemoryCard _secondRevealed;
    public bool canReveal { get { return _secondRevealed == null; } }

    public int _gridRows;
    public int _gridCols;
    public const float offsetX = 2.7f;
    public const float offsetY = 2.7f;
    [SerializeField] GameDifficulty game;
    [SerializeField] private MemoryCard originalCard;
    [SerializeField] private Sprite[] images;

    private void Start()
    {
        winMenu.SetActive(false);
        Vector3 startPos = originalCard.transform.position;

        //int[] easyNumbers = { 0, 0, 1, 1, 2, 2, 3, 3 };
        int[] mediumNumbers = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};
        int[] hardNumbers = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15};
        int[] insaneNumbers = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15,
        16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,25,25,26,26,27,27,28,28,29,29,30,30,31,31,32,32,33,33,34,34,35,35 };


        switch (game)
        {
            case GameDifficulty.Easy:
                SetGrid(startPos, mediumNumbers);
                Debug.Log("easy");
                break;
            case GameDifficulty.Medium:
                SetGrid(startPos, hardNumbers);
                Debug.Log("medium");
                break;
            case GameDifficulty.Hard:
                SetGrid(startPos, insaneNumbers);
                Debug.Log("hard");
                break;
        }
    }
    private void Update()
    {
        WinCondition();
    }
    private int[] SetGrid(Vector3 startPos, int[] anyNumbers)
    {
        anyNumbers = ShuffleArray(anyNumbers);
        SetCards(startPos, anyNumbers);
        return anyNumbers;
    }

    private void SetCards(Vector3 startPos, int[] numbers)
    {
        for (int i = 0; i < _gridCols; i++)
        {
            for (int j = 0; j < _gridRows; j++)
            {
                MemoryCard card;
                if (i == 0 && j == 0)
                {
                    card = originalCard;
                }
                else
                {
                    card = Instantiate(originalCard);
                }

                int index = j * _gridCols + i;
                int id = numbers[index];
                card.SetCard(id, images[id]);

                float posX = (offsetX * i) + startPos.x;
                float posY = -(offsetY * j) + startPos.y;
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }
    }

    private int[] ShuffleArray(int[] numbers)
    {
        int[] newArray = numbers.Clone() as int[];
        for(int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }
    
    public void CardRevealed(MemoryCard card)
    {
        if(_firstRevealed == null)
        {
            _firstRevealed = card;
        }
        else
        {
            _secondRevealed = card;
            StartCoroutine(CheckMatch());
        }
    }

    private IEnumerator CheckMatch()
    {
        if(_firstRevealed.id == _secondRevealed.id)
        {
            _winPoints++;
            _score += 5;
            scoreLabel.text = "Score: " + _score;
        }
        else
        {
            _score--;
            yield return new WaitForSeconds(.5f);
            _firstRevealed.Unreveal();
            _secondRevealed.Unreveal();
        }
        _firstRevealed = null;
        _secondRevealed = null;
    }
    private void WinCondition()
    {
        if (_winPoints == numberOfWin)
        {
            gameMenu.SetActive(false);
            winMenu.SetActive(true);
            finalScore.text = "Score: " + _score;
            switch (game)
            {
                case GameDifficulty.Easy:
                    if (PlayerPrefs.GetInt("EasyBestScore") <= _score)
                    {
                        PlayerPrefs.SetInt("EasyBestScore", _score);
                    }
                    break;
                case GameDifficulty.Medium:
                    if (PlayerPrefs.GetInt("MediumBestScore") <= _score)
                    {
                        PlayerPrefs.SetInt("MediumBestScore", _score);
                    }
                    break;
                case GameDifficulty.Hard:
                    if (PlayerPrefs.GetInt("HardBestScore") <= _score)
                    {
                        PlayerPrefs.SetInt("HardBestScore", _score);
                    }
                    break;
            }
        }
    }
}
