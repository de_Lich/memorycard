using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject mainPanel;
    [SerializeField] GameObject setDifficultyPanel;
    [SerializeField] GameObject scorePanel;
    public void StartGame()
    {
        mainPanel.SetActive(false);
        setDifficultyPanel.SetActive(true);
    }
    public void ShowScore()
    {
        mainPanel.SetActive(false);
        scorePanel.SetActive(true);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void StartEasy()
    {
        SceneManager.LoadScene("EasyGame");
    }
    public void StartMedium()
    {
        SceneManager.LoadScene("MediumGame");
    }
    public void StartHard()
    {
        SceneManager.LoadScene("HardGame");
    }
    public void BackToMenu()
    {
        scorePanel.SetActive(false);
        mainPanel.SetActive(true);
    }
}
