using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveResult : MonoBehaviour
{
    private int defaultScore = 0;
    [SerializeField] Text easyScoretxt;
    [SerializeField] Text mediumScoretxt;
    [SerializeField] Text hardScoretxt;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("EasyBestScore"))
        {
            easyScoretxt.text = "Easy level: " + PlayerPrefs.GetInt("EasyBestScore").ToString();
        }
        if (PlayerPrefs.HasKey("MediumBestScore"))
        {
            mediumScoretxt.text = "Medium level: " + PlayerPrefs.GetInt("MediumBestScore").ToString();
        }
        if (PlayerPrefs.HasKey("HardBestScore"))
        {
            hardScoretxt.text = "Hard level: " + PlayerPrefs.GetInt("HardBestScore").ToString();
        }
        else
        {
            easyScoretxt.text = "Easy level: " + defaultScore.ToString();
            // to do anothers
        }
    }
}
